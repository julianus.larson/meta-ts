From e2d65d1d3a7a8594983514f4208cb110d05122cd Mon Sep 17 00:00:00 2001
From: Ilias Apalodimas <ilias.apalodimas@linaro.org>
Date: Mon, 22 Aug 2022 15:27:07 +0300
Subject: [PATCH] tee: optee: rework TA bus scanning code

Late versions of OP-TEE support a pseudo bus.  TAs that behave as
hardware blocks (e.g TPM, RNG etc) present themselves on a bus which we can
scan.  Unfortunately U-Boot doesn't support that yet. It's worth noting
that we already have a workaround for RNG.  The details are in
commit 70812bb83da6 ("tee: optee: bind rng optee driver")

So let's add a list of devices based on U-Boot Kconfig options that we will
scan until we properly implement the tee-bus functionality.

Signed-off-by: Ilias Apalodimas <ilias.apalodimas@linaro.org>
---
 drivers/tee/optee/core.c | 22 +++++++++++++++++++---
 1 file changed, 19 insertions(+), 3 deletions(-)

diff --git a/drivers/tee/optee/core.c b/drivers/tee/optee/core.c
index a89d62aaf0b3..48a9b59171cb 100644
--- a/drivers/tee/optee/core.c
+++ b/drivers/tee/optee/core.c
@@ -31,6 +31,20 @@ struct optee_pdata {
 	optee_invoke_fn *invoke_fn;
 };
 
+static const struct {
+	const char *drv_name;
+	const char *dev_name;
+} optee_bus_probe[] = {
+#ifdef CONFIG_RNG_OPTEE
+	{ "optee-rng", "optee-rng" },
+#endif
+#ifdef CONFIG_TPM2_FTPM_TEE
+	{ "ftpm-tee", "ftpm-tee" },
+#endif
+};
+
+#define MAX_TEE_BUS_DEV_COUNT ARRAY_SIZE(optee_bus_probe)
+
 struct rpc_param {
 	u32	a0;
 	u32	a1;
@@ -643,7 +657,7 @@ static int optee_probe(struct udevice *dev)
 	struct optee_pdata *pdata = dev_get_plat(dev);
 	u32 sec_caps;
 	struct udevice *child;
-	int ret;
+	int ret, i;
 
 	if (!is_optee_api(pdata->invoke_fn)) {
 		dev_err(dev, "OP-TEE api uid mismatch\n");
@@ -672,8 +686,10 @@ static int optee_probe(struct udevice *dev)
 	 * in U-Boot, the discovery of TA on the TEE bus is not supported:
 	 * only bind the drivers associated to the supported OP-TEE TA
 	 */
-	if (IS_ENABLED(CONFIG_RNG_OPTEE)) {
-		ret = device_bind_driver(dev, "optee-rng", "optee-rng", &child);
+
+	for (i = 0; i < MAX_TEE_BUS_DEV_COUNT; i++) {
+		ret = device_bind_driver(dev, optee_bus_probe[i].drv_name,
+					 optee_bus_probe[i].dev_name, &child);
 		if (ret)
 			return ret;
 	}
-- 
2.34.1

